package utilities;

public class Constant {

	public static final String URL = "http://www.canada411.ca/";
	public static final String ChromeDriver_Exe_Path = System.getProperty("user.dir") + "\\Deps\\ChromeDriver\\chromedriver.exe";
	public static final String Excel_Data_Path = System.getProperty("user.dir") + "\\Canada411.xlsx";
	//public static final String File_TestData = "Canada411.xlsx";
}
