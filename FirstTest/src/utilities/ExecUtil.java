package utilities;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import org.apache.poi.xssf.usermodel.XSSFCell;
import org.apache.poi.xssf.usermodel.XSSFRow;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

public class ExecUtil {
	private static XSSFSheet ExcelWSheet;
	private static XSSFWorkbook ExcelWBook;
	private static XSSFCell Cell;
	private static XSSFRow Row;
	
	// This method is to set the File Path and to open the excel file, Pass Excel path and sheetname as arguments to this method
	public static void setExcelFile(String path, String SheetName) throws Exception
	{
		try{
			// Open the excel file
			FileInputStream ExcelFile = new FileInputStream(path);
			
			// Access the required test data sheet
			ExcelWBook = new XSSFWorkbook(ExcelFile);
			
			ExcelWSheet = ExcelWBook.getSheet(SheetName);
			
		}catch(Exception e)
		{
			throw(e);
		}
	}
	
	// This method is to read the test data from the excel cell, in this we are passing parameters as Row num and Col num
	public static String getCellData(int RowNum, int ColNum) throws Exception
	{
		try{
			Cell = ExcelWSheet.getRow(RowNum).getCell(ColNum);
			
			String CellData = Cell.getStringCellValue();
			
			return CellData;
		}
		catch(Exception e)
		{
			return "";
		}
	}
	
	// This method is to write in the Excel cell, Row num and Col num are the pararmeters
	public static void setCellData(String Result, int RowNum, int ColNum) throws Exception
	{
		try{
			Row = ExcelWSheet.getRow(RowNum);
			
			Cell = Row.getCell(ColNum, Row.RETURN_BLANK_AS_NULL);
			
			if(Cell == null)
			{
				Cell = Row.createCell(ColNum);
				Cell.setCellValue(Result);
			}
			else
			{
				Cell.setCellValue(Result);
			}
			
			FileOutputStream fileOut = new FileOutputStream(Constant.Excel_Data_Path);
			
			ExcelWBook.write(fileOut);
			
			fileOut.flush();
			fileOut.close();
		}catch(Exception e)
		{
			throw(e);
		}
	}
}
