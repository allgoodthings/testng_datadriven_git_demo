package utilities;

import java.sql.Date;
import java.text.SimpleDateFormat;

import org.openqa.selenium.WebDriver;

public class Utility {

	public static String getCurrentTimeStamp() {
		
		long yourmilliseconds = System.currentTimeMillis();
		SimpleDateFormat sdf = new SimpleDateFormat("EEE d MMM, yyyy HH:mm:ss");    
		Date resultdate = new Date(yourmilliseconds);
	    return sdf.format(resultdate);
	}
	
	public static void chromeSetup(WebDriver driver) 
	{
		System.setProperty("webdriver.chrome.driver",  Constant.ChromeDriver_Exe_Path);
	}
}
